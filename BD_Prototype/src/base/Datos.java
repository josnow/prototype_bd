package base;

public interface Datos {

	public Datos clonar();
	public String getNombre();
	public String getApellido();
	public int getDocumento();
	public String getEmail();
	public int getTelefono();
	public void setApellido(String ape);
	public void setNombre(String nom);
	public void setEmail(String ema);
	public void setTelefono(int tel);
	public void setDocumento(int doc);
}
