package graficos;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

import base.*;

public class User {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		ArrayList usuarios = new ArrayList<Usuario>();
		int ciclo=1;
		int id=0;
		
		while(ciclo==1) {
		
			Usuario user = new Usuario();
			
			user.setId(id);
			System.out.println("Nombre: ");
			user.setNombre(lector.next());
		
			System.out.println("Apellido: ");
			user.setApellido(lector.next());
		
			System.out.println("Documento: ");
			user.setDocumento(lector.nextInt());
		
			System.out.println("Telefono: ");
			user.setTelefono(lector.nextInt());
		
			System.out.println("Email: ");
			user.setEmail(lector.next());
		
			usuarios.add(user);
		
			System.out.println("Digite 1 para un nuevo ingreso: ");
			ciclo = lector.nextInt();
			id++;
		}
		
		System.out.println("\tNombre\t\tApellido\tDocumento\tTelefono\tEmail");
		for(int i=0; i<usuarios.size();i++) {
			System.out.println(usuarios.get(i).toString());
		}
		
		System.out.println("Seleccione un usuario a clonar: ");
		Usuario user = (Usuario) usuarios.get(lector.nextInt());
		Datos clon = user.clonar();
		
		System.out.println("Que va a cambiar del usuario ya existente: ");
		System.out.println("1. Nombre");
		System.out.println("2. Apellido");
		System.out.println("3. Documento");
		System.out.println("4. Telefono");
		System.out.println("5. Email");
		
		switch(lector.nextInt()) {
		
		case 1:
			clon.setNombre(lector.next());
			break;
		case 2:
			clon.setApellido(lector.next());
			break;
		case 3:
			clon.setDocumento(lector.nextInt());
			break;
		case 4:
			clon.setTelefono(lector.nextInt());
			break;
		case 5:
			clon.setEmail(lector.next());
			break;
		}
		
		usuarios.add(clon);

		System.out.println("\tNombre\t\tApellido\tDocumento\tTelefono\tEmail");
		for(int i=0; i<usuarios.size();i++) {
			System.out.println(usuarios.get(i).toString());
		}
	}

}
